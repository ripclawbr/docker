#
# Ubuntu Dockerfile
#
# https://github.com/dockerfile/ubuntu
#

# Pull base image.
FROM ubuntu:18.04

# Install.
RUN \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y build-essential && \
  apt-get install -y software-properties-common && \
  apt-get install -y curl git htop unzip && \
  apt-get install -y nginx && \
  apt-get install -y mysql-server

# Set environment variables.
ENV HOME /root

# Define working directory.
WORKDIR /root

# Define the entrypoint
ENTRYPOINT ["/bin/bash"]
